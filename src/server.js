const EmailProcessor = require('./email-processor');
const Reader = require('./reader');
const ReplierBot = require('./replier-bot');
const Poller = require('./poller');

// Set 1 hour timeout between polls
// note: this is previous request + processing time + timeout
var poller = new Poller(1 * 60 * 60 * 1000);

const processMail = function() {
    var reader = new Reader();
    reader.getUnreadMails(function(mail) {
        console.log('Reached Post processing zone');
        let processor = new EmailProcessor(mail.text);
        processor.extractCustomerInfo(function(customerInfo) {
            console.log(customerInfo);
            ReplierBot.reply(customerInfo, mail, function(type, response) {
                // Mark mail seen if reply is successful
                //reader.markEmailSeen(mail.messageId, function() {
                //    
                // });
                reader.disconnect();
            });
        });
    });
};

// Wait till the timeout sent our event to the EventEmitter
poller.onPoll(() => {
    console.log('triggered');

    // Read unread mails from the server
    processMail();
    // Go for the next poll
    poller.poll();
});

processMail();
poller.poll();
