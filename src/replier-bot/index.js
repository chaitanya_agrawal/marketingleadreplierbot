const EmailSender = require('../sender/email');
const SMSSender = require('../sender/sms');
const { createEvent } = require('../analytics');

const Replier = {
    reply: function(customerInfo, mail, successCallback) {
        if (customerInfo.email) {
            console.log('Reply on email');
            EmailSender.sendMail({
                'to': customerInfo.email,
                'subject': 'Test mail from Marketing Bot',
                'text': 'This is email content from auto-bot'
            }, function(response) {
                console.log(response);
                successCallback('email', response);
            }, function(err) {
                console.log(err);
                createEvent('email_sent_fail', {
                    'email': customerInfo.email,
                    'err': err
                });
            });
        }
        if (customerInfo.phone) {
            console.log('Reply on phone number');
            console.log(customerInfo.phone);
            /* TODO: uncomment in production. Code is tested.
            SMSSender.sendSMS({
                to: '8174070273', // customerInfo.phone,
                text: 'This is testing text sms'
            }, function(err, response) {
                if (err) {
                    console.log('Error in sending message. : ' + err);
                    createEvent('sms_sent_fail', {
                        'sms': customerInfo.phone,
                        'error': err
                    });
                } else {
                    console.log(response);
                    console.log('Message sent');
                }
            });
            */
           successCallback('sms', {});
        }
        if (!(customerInfo.phone || customerInfo.email)) {
            createEvent('no_customer_info', {
                'from': mail.from.text,
                'messageId': mail.messageId
            });
        }
    }
}

module.exports = Replier;
