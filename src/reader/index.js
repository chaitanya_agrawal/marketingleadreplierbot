const Imap = require('imap');
const Inspect = require('util').inspect;
const SimpleParser = require('mailparser').simpleParser;

const Config = require('../config');


const createConnection = function() {
    var gmailImap = new Imap({
        user: Config.user.email,
        password: Config.user.password,
        host: 'imap.gmail.com',
        port: 993,
        tls: true
    });
    console.log('Connection created');
    return gmailImap;
};

const unreadMail = function(imap, mailBox, postCallback) {
    imap.search([ 'UNSEEN' ], function(err, results) {
        if (err) throw err;
        var f = imap.fetch(results, { bodies: '' });
        f.on('message', function(msg, sequenceNumber) {
            console.log('Message #%d', sequenceNumber);
            var prefix = '(#' + sequenceNumber + ') ';
            msg.on('body', function(stream, info) {
                SimpleParser(stream, (err, mail) => {
                    if (err) {
                        postCallback('');    
                    }
                    console.log('Processing Ended');
                    postCallback(mail);
                });
            });
            /*
            msg.once('attributes', function(attrs) {
                console.log(prefix + 'Attributes: %s', Inspect(attrs, false, 8));
            }); */
            msg.once('end', function() {
                console.log(prefix + 'Finished');
            });
        });
        f.once('error', function(err) {
            console.log('Fetch error: ' + err);
        });
        f.once('end', function() {
            console.log('Done fetching all messages!');
        });
    });
};

class Reader {
    constructor() {
        this.gmailImap = createConnection();
    }

    _initConnection(imap, readyCallback) {
        imap.once('error', function(err) {
            console.log(err);
        });
        imap.once('end', function() {
            console.log('Connection ended');
        });
        imap.once('ready', readyCallback);
        console.log('Connection inited');
        imap.connect();
    }

    getUnreadMails(postCallback) {
        var imap = this.gmailImap;
        this._initConnection(imap, function() {
            imap.openBox('INBOX', true, function(err, box) {
                if (err) throw err;
                unreadMail(imap, box, postCallback.bind(this));
            });
        });
    }

    markEmailSeen(id, successCallback) {
        this.gmailImap.setFlags(id, ['\\Seen'], function(err) {
            if (err) {
                console.log(JSON.stringify(err, null, 2));
            } else {
                console.log("marked as read");
                successCallback();
            }
        });
    }

    disconnect() {
        this.gmailImap.end();
    }
};

module.exports = Reader;
