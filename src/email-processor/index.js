const { NerManager } = require('node-nlp');

const isBlacklistedEmailId = function(emailId) {
    return ['noreply.krishnaindustries@gmail.com',
            'noreply.krishnaindustries@gmail.com.'].includes(emailId);
};

class EmailProcessor {
    constructor(mail) {
        this.email = mail || '';
        this.customerInfo = {};
        this.manager = new NerManager({ threshold: 0.8 });
    }

    extractCustomerInfo(callback, lang = 'en') {
        // console.log(this.email);
        // Find entities for phone number and email
        this.manager.findEntities(this.email, lang).then(entities => {
            console.log('_extractCustomerInfo');
            // console.log(entities);
            this._setCustomerInfo(entities);
            callback(this.customerInfo);
        });
    }

    _setCustomerInfo(entities) {
        for (var index = 0; index < entities.length; index++) {
            let entity = entities[index];
            // Set customer information if entity available
            this._setCustomerPhone(entity);
            this._setCustomerEmail(entity);
        }
    }

    _setCustomerPhone(info) {
        if (!this.customerInfo.phone && info.entity === 'phonenumber') {
            let resolution = info.resolution;
            if (resolution.score > 0.1) {
                this.customerInfo.phone = resolution.value;
                console.log(this.customerInfo.phone);
            }
        }
    }

    _setCustomerEmail(info) {
        if (!this.customerInfo.email && info.entity === 'email') {
            let email = info.resolution.value;
            if (!isBlacklistedEmailId(email)) {
                this.customerInfo.email = email;
                console.log(this.customerInfo.email);
            }
        }
    }
};

module.exports = EmailProcessor;
