const Analytics = require('analytics-node');
const Config = require('../config');

const analytics = new Analytics(Config.user.analytics.writeKey);

const createEvent = function(eventName, properties) {
    let event = {
        event: eventName,
        userId: Config.user.analytics.userId
    };
    if (properties) {
        event.properties = properties;
    }
    analytics.track(event);
};

module.exports = { createEvent };