const Nexmo = require('nexmo');
const config = require('../../config');


const nexmo = new Nexmo({
    apiKey: config.user.nexmo.key,
    apiSecret: config.user.nexmo.secret
});

const _rectifyPhoneNumber = function(phoneNumber) {
    if (phoneNumber.length === 10) {
        phoneNumber = '91' + phoneNumber;
    }
    return phoneNumber;
}

const sendSMS = function(smsData, successCallback) {
    console.log('sendSMS');
    let phoneNumber = _rectifyPhoneNumber(config.user.phoneNumber);
    nexmo.message.sendSms(phoneNumber, _rectifyPhoneNumber(smsData.to), smsData.text, successCallback);
}

module.exports = { sendSMS };