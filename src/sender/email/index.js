const nodemailer = require('nodemailer');
const xoauth2 = require('xoauth2');

const config = require('../../config');

// TODO: create separate gmail transporter
const gmailTransporter = nodemailer.createTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    secure: 'true',
    port: '465',
    auth: {
        type: 'login',
        user: config.user.email,
        pass: config.user.password
    }
});

/*
    auth: {
        type: 'OAuth2', // Authentication type
        user: config.user.email,
        clientId: config.user.client.id,
        clientSecret: config.user.gmail.secret,
        refreshToken: config.user.gmail.token
    }
*/

const sendMail = function(mailData, successCallback, failCallback) {
    // Set From field in mail
    mailData.from = config.user.email;
    mailData.cc = 'chaitiagrawal@gmail.com';
    // Send mail using transporter
    gmailTransporter.sendMail(mailData, function(err, response) {
        if (err) {
            failCallback(err);
        } else {
            successCallback(response);
        }
        gmailTransporter.close();
    });
};

module.exports = { sendMail };
